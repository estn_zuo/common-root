/**
 * Copyright  2016  Pemass
 * All Right Reserved.
 */
package com.biloba.common.dao.jpa;

import com.biloba.common.core.util.BeanUtil;
import com.biloba.common.core.util.UUIDUtil;
import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.BaseDomain;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.enumeration.AvailableEnum;
import com.biloba.common.domain.pojo.DomainPage;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import javax.persistence.Query;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description: AbstractBaseDao
 * @Author: estn.zuo
 * @CreateTime: 2016-06-12 22:29
 */
public abstract class AbstractBaseDao implements BaseDao {

    /**
     * 分页查询默认起始页
     */
    private static final Integer DEFAULT_PAGE_INDEX = 0;

    /**
     * 分页查询每页最大数
     */
    private static final Integer MAX_PAGE_SIZE = 100;

    /**
     *
     */

    private static final Integer MIN_PAGE_SIZE = 1;

    /**
     * pageIndex错误信息
     */
    private static final String PAGE_INDEX_NUMBER_ERROR = "pageIndex must greater than 0";

    /**
     * pageSize错误信息
     */
    private static final String PAGE_SIZE_NUMBER_ERROR = "pageSize must greater than 0 and less than equal 100";

    private static final String BEAN_IS_NULL = "bean is null";

    @Override
    public <T extends BaseDomain> T refresh(T t) {
        getEntityManager().refresh(t);
        return t;
    }

    public <T extends BaseDomain> T merge(T t) {
        t.setUpdateTime(new Date());
        t.setVersion(t.getVersion() + 1);
        return getEntityManager().merge(t);
    }

    @Override
    public <T extends BaseDomain> int updateByVersion(Class<T> clazz, Object id, String updateFieldName, Object updateFieldNameValue, Integer version) {
        String update = "update " + clazz.getName() + " c set c." + updateFieldName + " = ?1 , version = version + 1 where c.available = 1 and c.id = ?2 and version= ?3";
        Query query = getEntityManager().createQuery(update);
        query.setParameter(1, updateFieldNameValue);
        query.setParameter(2, id);
        query.setParameter(3, version);
        return query.executeUpdate();
    }

    public <T extends BaseDomain> void persist(T t) {
        t.setCreateTime(new Date());
        t.setUpdateTime(new Date());
        t.setVersion(0);
        if (t.getAvailable() == null) {
            t.setAvailable(AvailableEnum.AVAILABLE);
        }
        if (t.getUuid() == null) {
            t.setUuid(UUIDUtil.randomWithoutBar());
        }
        getEntityManager().persist(t);
    }

    public <T extends BaseDomain> T getEntityById(Class<T> clazz, Object id) {
        return getEntityManager().find(clazz, id);
    }

    public <T extends BaseDomain> void removeEntity(T t) {
        getEntityManager().remove(getEntityManager().contains(t) ? t : getEntityManager().merge(t));
    }

    public <T extends BaseDomain> List<T> getEntitiesByField(Class<T> clazz, String fieldName, Object fieldValue) {
        String sql;
        Query query;
        if (fieldValue == null) {
            sql = "select c from " + clazz.getName() + " c where c.available = 1 and c." + fieldName + " is null order by c.updateTime desc";
            query = getEntityManager().createQuery(sql);
        } else {
            sql = "select c from " + clazz.getName() + " c where c.available = 1 and c." + fieldName + " = ?1 order by c.updateTime desc";
            query = getEntityManager().createQuery(sql);
            query.setParameter(1, fieldValue);
        }

        query.setFirstResult(DEFAULT_PAGE_INDEX);
        query.setMaxResults(MAX_PAGE_SIZE);

        return query.getResultList();
    }

    public <T extends BaseDomain> T getEntityByField(Class<T> clazz, String fieldName, Object fieldValue) {
        String sql = "select c from " + clazz.getName() + " c where c.available = 1 and c." + fieldName + " = ?1 order by c.updateTime desc";
        Query query = getEntityManager().createQuery(sql);
        query.setParameter(1, fieldValue);
        query.setFirstResult(DEFAULT_PAGE_INDEX);
        query.setMaxResults(MIN_PAGE_SIZE);
        List<T> ret = query.getResultList();
        if (ret == null || ret.size() < 1) {
            return null;
        }
        return ret.get(0);
    }

    public <T extends BaseDomain> T getEntityByField(Class<T> clazz, String fieldName, Object fieldValue, String orderByFieldName, OrderBy orderBy) {
        String sql = "select c from " + clazz.getName() + " c where c.available = 1 and c." + fieldName + " = ?1 order by c." + orderByFieldName + " " + orderBy;
        Query query = getEntityManager().createQuery(sql);
        query.setParameter(1, fieldValue);
        query.setFirstResult(DEFAULT_PAGE_INDEX);
        query.setMaxResults(MIN_PAGE_SIZE);
        List<T> ret = query.getResultList();
        if (ret == null || ret.size() < 1) {
            return null;
        }
        return ret.get(0);
    }

    public <T extends BaseDomain> List<T> getEntitiesByFieldList(Class<T> clazz, Map<String, Object> fieldNameValueMap) {

        fieldNameValueMap = this.removeNullValue(fieldNameValueMap);

        String sql = "select c from " + clazz.getName() + " c where c.available = 1 ";
        Set<String> fieldNames = fieldNameValueMap.keySet();
        Iterator<String> iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            sql = sql + " and c." + fieldName + " = ?" + i;
        }
        sql = sql + " order by c.updateTime desc";
        Query query = getEntityManager().createQuery(sql);
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            query.setParameter(i, fieldNameValueMap.get(fieldName));
        }

        query.setFirstResult(DEFAULT_PAGE_INDEX);
        query.setMaxResults(MAX_PAGE_SIZE);
        return query.getResultList();
    }

    public <T extends BaseDomain> Integer getEntityTotalCount(Class<T> clazz, String fieldName, Object fieldValue) {
        String sql = "select count(c) from " + clazz.getName() + " c where c.available = 1 and c." + fieldName + " = ?1";
        Query query = getEntityManager().createQuery(sql);
        query.setParameter(1, fieldValue);

        List result = query.getResultList();
        return ((Long) result.get(0)).intValue();
    }

    public <T extends BaseDomain> Integer getEntityTotalCount(Class<T> clazz) {
        String sql = "select count(c) from " + clazz.getName() + " c where c.available = 1 ";
        Query query = getEntityManager().createQuery(sql);

        List result = query.getResultList();
        return ((Long) result.get(0)).intValue();
    }

    public <T extends BaseDomain> DomainPage<T> getAllEntitiesByPage(Class<T> clazz, Integer pageIndex, Integer pageSize) {
        return getAllEntitiesByPage(clazz, "updateTime", OrderBy.DESC, pageIndex, pageSize);
    }

    public <T extends BaseDomain> DomainPage<T> getAllEntitiesByPage(Class<T> clazz, String orderByField, OrderBy orderBy, Integer pageIndex, Integer pageSize) {
        this.validatePageindexAndPagesize(pageIndex, pageSize);

        if (orderByField == null || orderByField.equals("")) {
            orderByField = "updateTime";
        }
        if (orderBy == null) {
            orderBy = OrderBy.DESC;
        }

        String sql = "select c from " + clazz.getName() + " c where c.available = 1 order by c." + orderByField + " " + orderBy.name();

        Query query = getEntityManager().createQuery(sql);
        query.setFirstResult((pageIndex - 1) * pageSize);
        query.setMaxResults(pageSize);
        List<T> resultList = query.getResultList();

        Integer totalCount = getEntityTotalCount(clazz);

        DomainPage<T> domainPage = new DomainPage<T>(pageIndex, pageSize, totalCount);
        domainPage.getDomains().addAll(resultList);

        return domainPage;
    }

    public <T extends BaseDomain> DomainPage<T> getEntitiesByPage(Class<T> clazz, String fieldName, Object fieldValue,
                                                                  Integer pageIndex, Integer pageSize) {
        this.validatePageindexAndPagesize(pageIndex, pageSize);

        String sql = "select c from " + clazz.getName() + " c where c.available = 1 and c." + fieldName
                + " = ?1  order by c.updateTime desc";

        Query query = getEntityManager().createQuery(sql);
        query.setParameter(1, fieldValue);
        query.setFirstResult((pageIndex - 1) * pageSize);
        query.setMaxResults(pageSize);
        List<T> resultList = query.getResultList();

        Integer totalCount = getEntityTotalCount(clazz, fieldName, fieldValue);
        DomainPage<T> domainPage = new DomainPage<T>(pageIndex, pageSize, totalCount);
        domainPage.getDomains().addAll(resultList);

        return domainPage;
    }


    public <T extends BaseDomain> DomainPage<T> getEntitiesPagesByFieldList(Class<T> clazz, Map<String, Object> fieldNameValueMap, Integer pageIndex, Integer pageSize) {
        this.validatePageindexAndPagesize(pageIndex, pageSize);
        fieldNameValueMap = this.removeNullValue(fieldNameValueMap);

        String sql = "select c from " + clazz.getName() + " c where c.available = 1  ";
        Set<String> fieldNames = fieldNameValueMap.keySet();
        Iterator<String> iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            sql = sql + " and c." + fieldName + " = ?" + i;
        }
        sql = sql + " order by c.updateTime desc";
        Query query = getEntityManager().createQuery(sql);
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            query.setParameter(i, fieldNameValueMap.get(fieldName));
        }
        query.setFirstResult((pageIndex - 1) * pageSize);
        query.setMaxResults(pageSize);
        List<T> resultList = query.getResultList();

        sql = "select count(c) from " + clazz.getName() + " c where c.available = 1  ";
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            if (i == 1) {
                sql = sql + " and c." + fieldName + " = ?" + i;
            } else {
                sql = sql + " and c." + fieldName + " = ?" + i;
            }
        }
        sql = sql + " order by c.updateTime desc";
        query = getEntityManager().createQuery(sql);
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            query.setParameter(i, fieldNameValueMap.get(fieldName));
        }
        Integer totalCount = ((Long) query.getResultList().get(0)).intValue();

        DomainPage<T> domainPage = new DomainPage<T>(pageIndex, pageSize, totalCount);
        domainPage.getDomains().addAll(resultList);
        return domainPage;
    }

    public <T extends BaseDomain> List<T> getEntitiesByFieldList(Class<T> clazz, Map<String, Object> fieldNameValueMap,
                                                                 String orderByFieldName, OrderBy orderBy) {

        fieldNameValueMap = this.removeNullValue(fieldNameValueMap);

        String sql = "select c from " + clazz.getName() + " c where c.available = 1  ";
        Set<String> fieldNames = fieldNameValueMap.keySet();
        Iterator<String> iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            sql = sql + " and c." + fieldName + " = ?" + i;
        }
        if (orderByFieldName == null || orderByFieldName.equals("")) {
            orderByFieldName = "updateTime";
        }
        if (orderBy == null) {
            orderBy = OrderBy.DESC;
        }
        sql = sql + " order by c." + orderByFieldName + " " + orderBy.name();
        Query query = getEntityManager().createQuery(sql);
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            query.setParameter(i, fieldNameValueMap.get(fieldName));
        }
        return query.getResultList();
    }

    public <T extends BaseDomain> DomainPage<T> getEntitiesPagesByFieldList(Class<T> clazz,
                                                                            Map<String, Object> fieldNameValueMap, String orderByFieldName, OrderBy orderBy, Integer pageIndex, Integer pageSize) {
        this.validatePageindexAndPagesize(pageIndex, pageSize);

        fieldNameValueMap = this.removeNullValue(fieldNameValueMap);

        String sql = "select c from " + clazz.getName() + " c where c.available = 1  ";
        Set<String> fieldNames = fieldNameValueMap.keySet();
        Iterator<String> iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            sql = sql + " and c." + fieldName + " = ?" + i;
        }
        sql = sql + " order by c." + orderByFieldName + " " + orderBy.name();
        Query query = getEntityManager().createQuery(sql);
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            query.setParameter(i, fieldNameValueMap.get(fieldName));
        }
        query.setFirstResult((pageIndex - 1) * pageSize);
        query.setMaxResults(pageSize);
        List<T> resultList = query.getResultList();

        sql = "select count(c) from " + clazz.getName() + " c where c.available = 1  ";
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            if (i == 1) {
                sql = sql + " and c." + fieldName + " = ?" + i;
            } else {
                sql = sql + " and c." + fieldName + " = ?" + i;
            }
        }
        query = getEntityManager().createQuery(sql);
        iterator = fieldNames.iterator();
        for (int i = 1; i <= fieldNames.size(); i++) {
            String fieldName = iterator.next();
            query.setParameter(i, fieldNameValueMap.get(fieldName));
        }
        Integer totalCount = ((Long) query.getResultList().get(0)).intValue();

        DomainPage<T> domainPage = new DomainPage<T>(pageIndex, pageSize, totalCount);
        domainPage.getDomains().addAll(resultList);
        return domainPage;
    }

    @Override
    public <T extends BaseDomain> List<T> getEntitiesByExpression(Class<T> clazz, Expression expression) {

        String jpql = "select c from " + clazz.getName() + " c where c.available = 1 ";
        if (expression != null) {
            jpql = jpql + " and " + expression.getFieldName() + " " + expression.getOperation().getValue() + " ?1";
        }
        jpql = jpql + " order by c.updateTime desc";

        Query query = getEntityManager().createQuery(jpql);
        this.settingExpressionParameterValue(expression, query);
        query.setFirstResult(DEFAULT_PAGE_INDEX);
        query.setMaxResults(MAX_PAGE_SIZE);
        return query.getResultList();

    }

    @Override
    public <T extends BaseDomain> List<T> getEntitiesByExpressionList(Class<T> clazz, List<Expression> expressions) {

        /*-- 拼接SQL --*/
        String jpql = "select c from " + clazz.getName() + " c where c.available = 1 ";
        if (expressions != null && expressions.size() > 0) {
            int position = 1;
            for (Expression expression : expressions) {
                jpql = jpql + " and c." + expression.getFieldName() + " " + expression.getOperation().getValue() + " ?" + position;
                position++;
            }
        }
        jpql = jpql + " order by c.updateTime desc";

         /*-- 设置参数 --*/
        Query query = getEntityManager().createQuery(jpql);
        this.settingExpressionParameterValue(expressions, query);
//        query.setMaxResults(MAX_PAGE_SIZE);

        return query.getResultList();
    }

    @Override
    public <T extends BaseDomain> DomainPage<T> getEntitiesPagesByExpression(Class<T> clazz, Expression expression, Integer pageIndex, Integer pageSize) {

        this.validatePageindexAndPagesize(pageIndex, pageSize);

        String jpql = "select c from " + clazz.getName() + " c where c.available = 1 ";
        if (expression != null) {
            jpql = jpql + " and c." + expression.getFieldName() + " " + expression.getOperation().getValue() + " ?1";
        }
        jpql = jpql + " order by c.updateTime desc";

        Query query = getEntityManager().createQuery(jpql);
        this.settingExpressionParameterValue(expression, query);
        query.setFirstResult((pageIndex - 1) * pageSize);
        query.setMaxResults(pageSize);
        List<T> resultList = query.getResultList();

        /*-- 求总条数 --*/
        jpql = "select count(c) from " + clazz.getName() + " c where c.available = 1 ";
        if (expression != null) {
            jpql = jpql + " and c." + expression.getFieldName() + " " + expression.getOperation().getValue() + " ?1";
        }
        jpql = jpql + " order by c.updateTime desc";

        query = getEntityManager().createQuery(jpql);
        this.settingExpressionParameterValue(expression, query);

        Integer totalCount = ((Long) query.getResultList().get(0)).intValue();

        DomainPage<T> domainPage = new DomainPage<T>(pageIndex, pageSize, totalCount);
        domainPage.getDomains().addAll(resultList);
        return domainPage;

    }


    @Override
    public <T extends BaseDomain> DomainPage<T> getEntitiesPagesByExpressionList(Class<T> clazz, List<Expression> expressions, Integer pageIndex, Integer pageSize) {
        return getEntitiesPagesByExpressionList(clazz, expressions, "updateTime", OrderBy.DESC, pageIndex, pageSize);
    }

    @Override
    public <T extends BaseDomain> DomainPage<T> getEntitiesPagesByExpressionList(Class<T> clazz, List<Expression> expressions, String orderByFieldName, OrderBy orderBy, Integer pageIndex, Integer pageSize) {
        this.validatePageindexAndPagesize(pageIndex, pageSize);

        /*-- 拼接SQL --*/
        String jpql = "select c from " + clazz.getName() + " c where c.available = 1 ";
        if (expressions != null && expressions.size() > 0) {
            int position = 1;
            for (Expression expression : expressions) {
                jpql = jpql + " and c." + expression.getFieldName() + " " + expression.getOperation().getValue() + " ?" + position;
                position++;
            }
        }
        jpql = jpql + " order by c." + orderByFieldName + "  " + orderBy.toString();

         /*-- 设置参数 --*/
        Query query = getEntityManager().createQuery(jpql);
        this.settingExpressionParameterValue(expressions, query);
        query.setFirstResult((pageIndex - 1) * pageSize);
        query.setMaxResults(pageSize);
        List<T> resultList = query.getResultList();

        /*-- 求总条数 --*/
        jpql = "select count(c) from " + clazz.getName() + " c where c.available = 1 ";
        if (expressions != null && expressions.size() > 0) {
            int position = 1;
            for (Expression expression : expressions) {
                jpql = jpql + " and c." + expression.getFieldName() + " " + expression.getOperation().getValue() + " ?" + position;
                position++;
            }
        }
        jpql = jpql + " order by c." + orderByFieldName + "  " + orderBy.toString();

        query = getEntityManager().createQuery(jpql);
        this.settingExpressionParameterValue(expressions, query);
        Integer totalCount = ((Long) query.getResultList().get(0)).intValue();

        DomainPage<T> domainPage = new DomainPage<T>(pageIndex, pageSize, totalCount);
        domainPage.getDomains().addAll(resultList);
        return domainPage;

    }

    @Override
    public <T extends BaseDomain> List<T> getEntitiesByBean(Class<T> clazz, T bean) {
        Preconditions.checkNotNull(bean, BEAN_IS_NULL);
        Map<String, Object> map = BeanUtil.convert$Map(bean);
        map.remove("available");
        return this.getEntitiesByFieldList(clazz, map);
    }

    @Override
    public <T extends BaseDomain> List<T> getEntitiesByBean(Class<T> clazz, T bean, String orderByFieldName, OrderBy orderBy) {
        Preconditions.checkNotNull(bean, BEAN_IS_NULL);
        Map<String, Object> map = BeanUtil.convert$Map(bean);
        map.remove("available");
        return this.getEntitiesByFieldList(clazz, map, orderByFieldName, orderBy);
    }

    @Override
    public <T extends BaseDomain> DomainPage<T> getEntitiesPagesByBean(Class<T> clazz, T bean, Integer pageIndex, Integer pageSize) {
        Preconditions.checkNotNull(bean, BEAN_IS_NULL);
        Map<String, Object> map = BeanUtil.convert$Map(bean);
        map.remove("available");
        return this.getEntitiesPagesByFieldList(clazz, map, pageIndex, pageSize);
    }

    @Override
    public <T extends BaseDomain> DomainPage<T> getEntitiesPagesByBean(Class<T> clazz, T bean, String orderByFieldName, OrderBy orderBy, Integer pageIndex, Integer pageSize) {
        Preconditions.checkNotNull(bean, BEAN_IS_NULL);
        Map<String, Object> map = BeanUtil.convert$Map(bean);
        map.remove("available");
        return this.getEntitiesPagesByFieldList(clazz, map, orderByFieldName, orderBy, pageIndex, pageSize);
    }

    private void settingExpressionParameterValue(Expression expression, Query query) {
        if (expression != null) {
            Object value = expression.getFieldValue();
            if ("like".equals(expression.getOperation().getValue())) {
                switch (expression.getOperation()) {
                    case ALL_LIKE:
                        value = "%" + value + "%";
                        break;
                    case LEFT_LIKE:
                        value = "%" + value;
                        break;
                    case RIGHT_LIKE:
                        value = value + "%";
                        break;
                }
            }
            query.setParameter(1, value);
        }
    }

    private void settingExpressionParameterValue(List<Expression> expressions, Query query) {
        if (expressions != null && expressions.size() > 0) {
            int position = 1;
            for (Expression expression : expressions) {
                Object value = expression.getFieldValue();
                if ("like".equals(expression.getOperation().getValue())) {
                    switch (expression.getOperation()) {
                        case ALL_LIKE:
                            value = "%" + value + "%";
                            break;
                        case LEFT_LIKE:
                            value = "%" + value;
                            break;
                        case RIGHT_LIKE:
                            value = value + "%";
                            break;
                    }
                }
                query.setParameter(position, value);
                position++;
            }
        }
    }


    /**
     * 校验pageIndex和pageSize参数
     *
     * @param pageIndex
     * @param pageSize
     */
    private void validatePageindexAndPagesize(Integer pageIndex, Integer pageSize) {
        Preconditions.checkNotNull(pageIndex, "pageIndex is null");
        Preconditions.checkNotNull(pageSize, "pageSize is null");
        Preconditions.checkArgument(pageIndex > 0, PAGE_INDEX_NUMBER_ERROR);
        Preconditions.checkArgument(pageSize >= 1 && pageSize <= 100, PAGE_SIZE_NUMBER_ERROR);
    }

    /**
     * 删除Map集合中value为空的
     *
     * @param fieldNameValueMap
     * @return
     */
    private Map<String, Object> removeNullValue(Map<String, Object> fieldNameValueMap) {

        if (fieldNameValueMap == null) {
            return null;
        }

        Map<String, Object> map = Maps.newHashMap();
        Set<String> keySet = fieldNameValueMap.keySet();
        for (String key : keySet) {
            Object value = fieldNameValueMap.get(key);
            if (value != null) {
                map.put(key, value);
            }
        }
        return map;
    }

}

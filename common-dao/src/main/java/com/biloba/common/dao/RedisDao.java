/**
 * Copyright  2016  Pemass
 * All Right Reserved.
 */
package com.biloba.common.dao;

import com.biloba.common.domain.RedisDomain;
import com.biloba.common.domain.pojo.DomainPage;
import org.redisson.api.RedissonClient;

import java.util.Date;

/**
 * @Description: RedisDao
 * @Author: estn.zuo
 * @CreateTime: 2016-06-18 17:46
 */
public interface RedisDao {

    RedissonClient getRedissonClient();

    /**
     * 获取该Entity的ID序号
     *
     * @param clazz
     * @return
     */
    Long getSequence(Class clazz);

    /**
     * 保存对象
     *
     * @param v
     * @param <V>
     * @return
     */
    <V extends RedisDomain> V set(V v);

    /**
     * 保存对象并设置过期时间段
     *
     * @param v
     * @param expire
     * @param <V>
     * @return
     */
    <V extends RedisDomain> V set(V v, Long expire);

    /**
     * 保存对象并设置过期时间点
     *
     * @param v
     * @param expireAt
     * @param <V>
     * @return
     */
    <V extends RedisDomain> V set(V v, Date expireAt);

    /**
     * 获取对象
     *
     * @param clazz
     * @param id
     * @param <V>
     * @return
     */
    <V extends RedisDomain> V get(Class clazz, Long id);

    /**
     * 更新对象信息
     *
     * @param v
     * @param <V>
     * @return
     */
    <V extends RedisDomain> V update(V v);

    /**
     * 删除指定对象
     *
     * @param clazz
     * @param id
     * @return
     */
    Boolean delete(Class clazz, Long id);

    /**
     * 分页获取对象信息
     *
     * @param clazz
     * @param pageIndex
     * @param pageSize
     * @param <V>
     * @return
     */
    <V extends RedisDomain> DomainPage<V> find(Class clazz, Integer pageIndex, Integer pageSize);



}

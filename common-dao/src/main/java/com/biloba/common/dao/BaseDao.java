package com.biloba.common.dao;


import com.biloba.common.domain.BaseDomain;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

/**
 * 1.本接口中，没有具体说明排序字段及排序方式是，默认采用更新时间降序排列
 * <p/>
 * 2.当需要扩展本接口时，可以直接继承本类就OK
 * <p/>
 */
public interface BaseDao {

    enum OrderBy {
        DESC,
        ASC
    }


    /**
     * 获取EntityManager
     *
     * @return
     */
    EntityManager getEntityManager();


    /**
     * 更新对象，如果采用JPA实现DAO，这里将从数据库重新获取数据更新内存中的缓存
     *
     * @param domain
     * @param <T>
     * @return
     */
    <T extends BaseDomain> T refresh(T domain);

    /**
     * 保存对象，该对象可以是已持久化的对象，即可以完成对已有对象的数据更新
     *
     * @param t
     * @param <T>
     * @return
     */
    <T extends BaseDomain> T merge(T t);


    /**
     * 根据Version进行更新
     *
     * @param clazz
     * @param updateFieldName
     * @param updateFieldNameValue
     * @param version
     * @param <T>
     * @return
     */
    <T extends BaseDomain> int updateByVersion(Class<T> clazz, Object id, String updateFieldName, Object updateFieldNameValue, Integer version);


    /**
     * 保存一个新对象，该对象不是已持久化的对象，也即主键为空或者主键在数据库中不存在。如果相同的主键数据已存在则会抛出异常
     *
     * @param t
     * @param <T>
     */
    <T extends BaseDomain> void persist(T t);

    /**
     * 删除指定实体
     *
     * @param t
     * @param <T>
     */
    <T extends BaseDomain> void removeEntity(T t);

    /**
     * 根据对象的ID查询对象
     *
     * @param clazz
     * @param id
     * @param <T>
     * @return
     */
    <T extends BaseDomain> T getEntityById(Class<T> clazz, Object id);


    /**
     * 根据对象的属性名和值完全匹配查询一个或多个实体
     *
     * @param clazz
     * @param fieldName
     * @param fieldValue
     * @param <T>
     * @return
     */
    <T extends BaseDomain> List<T> getEntitiesByField(Class<T> clazz, String fieldName, Object fieldValue);

    /**
     * 根据对象的属性名和值完全匹配查询一个实体
     *
     * @param clazz
     * @param fieldName
     * @param fieldValue
     * @param <T>
     * @return
     */
    <T extends BaseDomain> T getEntityByField(Class<T> clazz, String fieldName, Object fieldValue);


    /**
     * 根据对象的属性名和值完全匹配查询一个实体
     *
     * @param clazz
     * @param fieldName
     * @param fieldValue
     * @param orderByFieldName 排序的属性名称
     * @param orderBy          排序字段的排序方式
     * @return
     */
    <T extends BaseDomain> T getEntityByField(Class<T> clazz, String fieldName, Object fieldValue, String orderByFieldName, OrderBy orderBy);

    /**
     * 根据对象的属性集合查询对象 ,每个属性值采用精确匹配
     *
     * @param clazz             对象类
     * @param fieldNameValueMap 对象属性名称值对集合
     * @param <T>
     * @return
     */
    <T extends BaseDomain> List<T> getEntitiesByFieldList(Class<T> clazz, Map<String, Object> fieldNameValueMap);

    /**
     * 根据对象属性名及名称完全匹配查询符合条件的实体记录总数
     *
     * @param clazz
     * @param fieldName  做完全匹配查询的属性名
     * @param fieldValue 做完全匹配查询的属性值
     * @param <T>
     * @return
     */
    <T extends BaseDomain> Integer getEntityTotalCount(Class<T> clazz, String fieldName, Object fieldValue);

    /**
     * 获取指定实体的记录数量
     *
     * @param clazz
     * @param <T>
     * @return
     */
    <T extends BaseDomain> Integer getEntityTotalCount(Class<T> clazz);

    /**
     * 分页获取所有的实体对象
     *
     * @param clazz
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getAllEntitiesByPage(Class<T> clazz, Integer pageIndex, Integer pageSize);

    /**
     * 分页获取所有的实体对象
     *
     * @param clazz
     * @param orderByField
     * @param orderBy
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getAllEntitiesByPage(Class<T> clazz, String orderByField, OrderBy orderBy, Integer pageIndex, Integer pageSize);

    /**
     * 根据对象属性，分页获取所有匹配的实体对象，默认按数据的更新时间排序
     *
     * @param clazz
     * @param fieldName
     * @param fieldValue
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesByPage(Class<T> clazz, String fieldName, Object fieldValue, Integer pageIndex, Integer pageSize);

    /**
     * 根据对象的属性集合查询对象 ,每个属性值采用精确匹配
     *
     * @param clazz             对象类
     * @param fieldNameValueMap 对象属性名称值对集合
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesPagesByFieldList(Class<T> clazz, Map<String, Object> fieldNameValueMap, Integer pageIndex, Integer pageSize);

    /**
     * 根据对象的属性集合查询对象，且根据参数进行升降序排序，每个属性值采用精确匹配
     *
     * @param clazz
     * @param fieldNameValueMap
     * @param orderByFieldName
     * @param orderBy
     * @param <T>
     * @return
     */
    <T extends BaseDomain> List<T> getEntitiesByFieldList(Class<T> clazz, Map<String, Object> fieldNameValueMap, String orderByFieldName, OrderBy orderBy);

    /**
     * 根据对象的属性集合查询对象，且根据参数进行升降序排序，每个属性值采用精确匹配
     *
     * @param clazz
     * @param fieldNameValueMap
     * @param orderByFieldName
     * @param orderBy
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesPagesByFieldList(Class<T> clazz, Map<String, Object> fieldNameValueMap, String orderByFieldName, OrderBy orderBy, Integer pageIndex, Integer pageSize);


    //*****************------根据expression来查询------**********************

    /**
     * 根据查询表达式查询对象
     *
     * @param clazz
     * @param expression
     * @param <T>
     * @return
     */
    <T extends BaseDomain> List<T> getEntitiesByExpression(Class<T> clazz, Expression expression);

    /**
     * 根据查询表达式集合查询对象，每个表达式直接使用and连接
     *
     * @param clazz
     * @param expressions
     * @param <T>
     * @return
     */
    <T extends BaseDomain> List<T> getEntitiesByExpressionList(Class<T> clazz, List<Expression> expressions);

    /**
     * 根据查询表达式分页查询对象
     *
     * @param clazz
     * @param expression
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesPagesByExpression(Class<T> clazz, Expression expression, Integer pageIndex, Integer pageSize);

    /**
     * 根据查询表达式集合分页查询对象，每个表达式直接使用and连接
     *
     * @param clazz
     * @param expressions
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesPagesByExpressionList(Class<T> clazz, List<Expression> expressions, Integer pageIndex, Integer pageSize);

    /**
     * 根据查询表达式集合分页查询对象，每个表达式直接使用and连接
     *
     * @param clazz
     * @param expressions
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesPagesByExpressionList(Class<T> clazz, List<Expression> expressions, String orderByFieldName, OrderBy orderBy, Integer pageIndex, Integer pageSize);


    //*****************------根据BEAN来查询------**********************

    /**
     * 根据Bean查询数据
     * 查询条件为bean中不为空的字段
     *
     * @param clazz
     * @param bean
     * @param <T>
     * @return
     */
    <T extends BaseDomain> List<T> getEntitiesByBean(Class<T> clazz, T bean);

    /**
     * 根据Bean查询数据
     * 查询条件为bean中不为空的字段
     *
     * @param clazz
     * @param bean
     * @param orderByFieldName 排序字段
     * @param orderBy          排序方式
     * @param <T>
     * @return
     */
    <T extends BaseDomain> List<T> getEntitiesByBean(Class<T> clazz, T bean, String orderByFieldName, OrderBy orderBy);

    /**
     * 根据Bean分页查询数据
     * 查询条件为bean中不为空的字段
     *
     * @param clazz
     * @param bean
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesPagesByBean(Class<T> clazz, T bean, Integer pageIndex, Integer pageSize);

    /**
     * 根据Bean分页查询数据
     * 查询条件为bean中不为空的字段
     *
     * @param clazz
     * @param bean
     * @param orderByFieldName 排序字段
     * @param orderBy          排序方式
     * @param pageIndex
     * @param pageSize
     * @param <T>
     * @return
     */
    <T extends BaseDomain> DomainPage<T> getEntitiesPagesByBean(Class<T> clazz, T bean, String orderByFieldName, OrderBy orderBy, Integer pageIndex, Integer pageSize);
}
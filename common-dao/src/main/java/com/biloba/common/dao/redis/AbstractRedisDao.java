/**
 * Copyright  2016  Pemass
 * All Right Reserved.
 */
package com.biloba.common.dao.redis;

import com.biloba.common.core.constant.SystemConst;
import com.biloba.common.core.exception.BaseException;
import com.biloba.common.core.util.UUIDUtil;
import com.biloba.common.dao.RedisDao;
import com.biloba.common.domain.RedisDomain;
import com.biloba.common.domain.pojo.DomainPage;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RBucket;
import org.redisson.api.RBuckets;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Description: AbstractRedisDao
 * @Author: estn.zuo
 * @CreateTime: 2016-06-18 21:38
 */
public abstract class AbstractRedisDao implements RedisDao {

    private static final String ID = "ID";
    private static final String ENTITY_IS_NULL = "entity is null";
    private static final String ID_IS_NULL = "id is null";

    @Override
    public Long getSequence(Class clazz) {
        String key = clazz.getName() + SystemConst.REDIS_KEY_SEPARATOR_SYMBOL + ID;
        RAtomicLong id = getRedissonClient().getAtomicLong(key);
        return id.get();
    }

    @Override
    public <V extends RedisDomain> V set(V v) {
        return this.set(v, -1L);
    }

    @Override
    public <V extends RedisDomain> V set(V v, Long expire) {
        Preconditions.checkNotNull(v, ENTITY_IS_NULL);
        v = this.init(v);
        String key = getKey(v, v.getId());
        RBucket<V> bucket = getRedissonClient().getBucket(key);
        if (new Long("-1").equals(expire)) {
            bucket.expire(expire, TimeUnit.SECONDS);
        }
        bucket.set(v);
        return v;
    }

    @Override
    public <V extends RedisDomain> V set(V v, Date expireAt) {
        Preconditions.checkNotNull(v, ENTITY_IS_NULL);
        Preconditions.checkNotNull(expireAt);
        v = this.init(v);
        String key = getKey(v, v.getId());
        RBucket<V> bucket = getRedissonClient().getBucket(key);
        bucket.expireAt(expireAt);
        bucket.set(v);
        return v;
    }

    @Override
    public <V extends RedisDomain> V get(Class clazz, Long id) {
        Preconditions.checkNotNull(id, ID_IS_NULL);
        Preconditions.checkArgument(id > 0);
        String key = clazz.getName() + SystemConst.REDIS_KEY_SEPARATOR_SYMBOL + id;
        RBucket<V> bucket = getRedissonClient().getBucket(key);
        if (bucket != null) {
            return bucket.get();
        }
        return null;
    }

    @Override
    public <V extends RedisDomain> V update(V v) {
        Preconditions.checkNotNull(v, ENTITY_IS_NULL);
        Long id = v.getId();
        if (id == null || id < 0) {
            throw new BaseException("update entity exception");
        }

        String key = getKey(v, id);
        RBucket<V> bucket = getRedissonClient().getBucket(key);
        if (bucket == null) {
            throw new BaseException("entity not found");
        }
        v.setUpdateTime(new Date());
        bucket.set(v);
        return v;
    }


    @Override
    public Boolean delete(Class clazz, Long id) {
        Preconditions.checkNotNull(id, ID_IS_NULL);
        Preconditions.checkArgument(id > 0);

        String key = clazz.getName() + SystemConst.REDIS_KEY_SEPARATOR_SYMBOL + id;
        RBucket<Object> bucket = getRedissonClient().getBucket(key);
        return bucket == null || bucket.delete();
    }

    @Override
    public <V extends RedisDomain> DomainPage<V> find(Class clazz, Integer pageIndex, Integer pageSize) {

        String key = clazz.getName() + SystemConst.REDIS_KEY_SEPARATOR_SYMBOL + "[0-9]*";

        RBuckets buckets = getRedissonClient().getBuckets();
        List<RBucket<V>> rBuckets = buckets.find(key);

        DomainPage<V> domainPage = new DomainPage<>();


        Collections.sort(rBuckets, new Comparator<RBucket<V>>() {
            @Override
            public int compare(RBucket<V> o1, RBucket<V> o2) {
                return (int) (o2.get().getId() - o1.get().getId());
            }
        });

        List<V> domain = Lists.newArrayList();
        domainPage.setTotalCount(Integer.parseInt(rBuckets.size() + ""));
        for (int i = (pageIndex - 1) * pageSize; (i < pageIndex * pageSize) && (i < domainPage.getTotalCount()); i++) {
            RBucket<V> rBucket = rBuckets.get(i);
            domain.add(rBucket.get());
        }
        domainPage.setDomains(domain);
        domainPage.setPageIndex(pageIndex);
        domainPage.setPageSize(pageSize);
        domainPage.setPageCount(Integer.parseInt(domain.size() + ""));

        return domainPage;
    }

    /**
     * 初始化V
     *
     * @param v
     * @return
     */
    private <V extends RedisDomain> V init(V v) {
        String key = v.getClass().getName() + SystemConst.REDIS_KEY_SEPARATOR_SYMBOL + ID;

        RAtomicLong id = getRedissonClient().getAtomicLong(key);
        long l = id.incrementAndGet();
        v.setId(l);

        v.setCreateTime(new Date());
        v.setUpdateTime(new Date());
        v.setUuid(UUIDUtil.randomWithoutBar());

        return v;
    }

    private <V extends RedisDomain> String getKey(V v, Long id) {
        return v.getClass().getName() + SystemConst.REDIS_KEY_SEPARATOR_SYMBOL + id;
    }


}

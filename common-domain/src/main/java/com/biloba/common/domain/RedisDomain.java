/**
 * Copyright  2016  Pemass
 * All Right Reserved.
 */
package com.biloba.common.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: Redis实体基础类
 * @Author: estn.zuo
 * @CreateTime: 2016-06-18 18:26
 */
public abstract class RedisDomain implements Serializable, Cloneable {

    /**
     * ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * UUID
     */
    private String uuid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}

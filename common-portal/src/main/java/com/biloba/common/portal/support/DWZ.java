package com.biloba.common.portal.support;

public interface DWZ {

    String REL = "rel";

    String PAGE_TITLE = "pageTitle";

    String VIEW = "view";

    String LIST = "list";

    String ADD = "add";

    String EDIT = "edit";

    String DELETE = "delete";

    String DETAIL = "detail";

    String STATESCODE = "statusCode";

    String MESSAGE = "message";

    String ADD_SUCCESS = "添加成功";

    String EDIT_SUCCESS = "编辑成功";

    String DELETE_SUCCESS = "删除成功";

    String OPR_SUCCESS = "操作成功";

    String CLOSE_CURRENT = "closeCurrent";

    String[] RESULT_KEYS = new String[]{"navTabId", "rel", "callbackType", "forwardUrl"};

    String[] COMMON_PARAMS = new String[]{"rel", "pageTitle", "pageIndex", "pageSize"};
}
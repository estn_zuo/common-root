package com.biloba.common.portal.htmlparse;

import org.htmlparser.tags.CompositeTag;

public class BrTagNode extends CompositeTag {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String[] mIds = new String[] { "br" };

	/**
	 * The set of end tag names that indicate the end of this tag.
	 */
	private static final String[] mEndTagEnders = new String[] { "br" };

	/**
	 * Return the set of names handled by this tag.
	 * 
	 * @return The names to be matched that create tags of this type.
	 */
	public String[] getIds() {
		return (mIds);
	}

	public String[] getEndTagEnders() {
		return (mEndTagEnders);
	}
}

/**
 * Copyright  2016  Pemass
 * All Right Reserved.
 */
package com.biloba.common.portal.interceptor;

import com.biloba.common.portal.support.DWZ;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @Description: 通用参数返回model拦截器
 * @Author: estn.zuo
 * @CreateTime: 2017-03-23 16:24
 */
public class CommonParamInterceptor extends HandlerInterceptorAdapter {

    public static ThreadLocal<Map<String, Object>> mapThreadLocal = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Map<String, Object> paramMap = null;

        for (String param : DWZ.COMMON_PARAMS) {
            String value = request.getParameter(param);
            if (!Strings.isNullOrEmpty(value)) {
                if (paramMap == null) {
                    paramMap = Maps.newHashMap();
                }
                paramMap.put(param, value);
            }
        }

        if (paramMap != null) {
            mapThreadLocal.set(paramMap);
        }

        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        Map<String, Object> map = mapThreadLocal.get();
        if (map != null) {
            if (modelAndView != null && modelAndView.getModel() != null)
                modelAndView.getModel().putAll(map);
        }
        mapThreadLocal.remove();
    }

    /**
     * This implementation is empty.
     */
    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        mapThreadLocal.remove();
    }
}

package com.biloba.common.portal.htmlparse;

import com.biloba.common.core.constant.ConfigurationConst;
import com.biloba.common.core.pojo.Body;
import com.biloba.common.core.pojo.BodyType;
import com.biloba.common.core.util.FileUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.PrototypicalNodeFactory;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HtmlUtil {

    private static Logger logger = LoggerFactory.getLogger(HtmlUtil.class);

    private static ObjectMapper objectMapper = new ObjectMapper();


    /**
     * Body内容转换成Html字符串
     *
     * @param details
     * @return
     */
    public static String bodyToHtml(List<Body> details) {

        if (details == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(20);
        try {
            for (Body body : details) {
                if (BodyType.TXT.equals(body.getType())) {
                    sb.append("<p>").append(body.getValue()).append("</p>");
                } else if (BodyType.IMAGE.equals(body.getType())) {
                    String url = body.getValue();
                    if (!url.startsWith("http://")) {
                        url = ConfigurationConst.RESOURCE_ROOT_URL + url;
                    }
                    sb.append("<p>").append("<img src='" + url + "'  />").append("</p>");
                } else if (BodyType.VIDEO.equals(body.getType())) {
                    sb.append("<p>").append("<a href='" + body.getValue() + "'>").append(body.getTitle()).append("</a>").append("</p>");
                }
                sb.append("<p></p>");
            }
        } catch (Exception e) {
            logger.error("parse json error");
        }
        return sb.toString();
    }

    /**
     * JSON数组转换成Html字符串
     *
     * @param jsonValue
     * @return
     */

    public static String jsonTohtml(String jsonValue) {
        try {
            List<Body> details = objectMapper.readValue(jsonValue, new TypeReference<List<Body>>() {
            });
            return bodyToHtml(details);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Html内容转换成Json字符串
     *
     * @param htmlValue
     * @return
     */
    public static String htmlToJson(String htmlValue) {
        List<Body> bodyList = htmlToBody(htmlValue);
        try {
            return objectMapper.writeValueAsString(bodyList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Html内容转换成Body
     *
     * @param htmlValue
     * @return
     */
    public static List<Body> htmlToBody(String htmlValue) {

        List<Body> bodyList = new ArrayList<Body>();

        if (htmlValue == null) {
            return null;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("parse html to json, html=" + htmlValue);
        }

        try {
            List<Body> editorValueList = toBodyList(htmlValue);
            for (Body body : editorValueList) {

                // value不能为空
                String bodyValue = body.getValue();
                if (StringUtils.isBlank(bodyValue)) {
                    continue;
                }

                Body b = new Body();
                b.setValue(bodyValue);
                b.setType(body.getType());
                b.setTitle(body.getTitle());

                //如果类型为IMG，则对IMG的URL进行处理
                if (BodyType.IMAGE.equals(body.getType())) {
                    String relativePath = doFetchInternalPath(bodyValue);
                    if (relativePath.startsWith("http")) {
                        b.setValue(FileUtil.saveFile(relativePath, relativePath.substring(relativePath.lastIndexOf(".") + 1)));
                    } else {
                        b.setValue(relativePath);
                    }
                }

                bodyList.add(b);
            }

        } catch (Exception e) {
            logger.error("parse html  error");
        }

        if (bodyList.size() == 0) {
            bodyList = null;
        }

        return bodyList;
    }

    /**
     * 判断该URL是否为系统内部的URL
     * 若为则返回系统内部的URL路径,否则返回全路径
     *
     * @param url
     * @return
     */
    private static String doFetchInternalPath(String url) {
        String[] segments = url.split("/");
        int length = segments.length;
        if (length < 4) {
            return url;
        }

        String filename = segments[length - 1];
        String dir = segments[length - 2];
        String parentDir = segments[length - 3];

        //根据生成图片URL地址的规则,判断是否为我们系统自己的图片，用于返回相对路径
        if (filename.length() < 4) {
            return url;
        }

        if (filename.substring(0, 2).equals(parentDir) && filename.substring(2, 4).equals(dir)) {
            return "/" + segments[length - 4] + "/" + segments[length - 3] + "/" + segments[length - 2] + "/" + segments[length - 1];
        }
        return url;
    }

    /**
     * 将html内容转换成Body集合
     *
     * @param htmlValue
     * @return
     */
    public static List<Body> toBodyList(String htmlValue) {
        return toBodyList(htmlValue, null);
    }

    public static List<Body> toBodyList(String htmlValue, List<Body> contents) {
        if (htmlValue == null) {
            return null;
        }
        if (contents == null) {
            contents = new ArrayList<Body>();
        }
        htmlValue = htmlValue.replaceAll("&nbsp;", " ");
        Parser parser = new Parser();
        PrototypicalNodeFactory nodeFactory = new PrototypicalNodeFactory();
        nodeFactory.registerTag(new BrTagNode());
        parser.setNodeFactory(nodeFactory);
        try {
            parser.setInputHTML(htmlValue);

            NodeList nodeList = parser.extractAllNodesThatMatch(new NodeFilter() {
                private static final long serialVersionUID = 1L;

                @Override
                public boolean accept(Node node) {
                    return !(node instanceof BrTagNode);
                }
            });

            getChildNode(contents, nodeList);
        } catch (ParserException e) {
            e.printStackTrace();
        }
        return contents.size() == 0 ? null : contents;
    }

    private static void getChildNode(List<Body> bodys, NodeList nodeList) {
        for (int i = 0; i < nodeList.size(); i++) {
            Node node = nodeList.elementAt(i);

            if (node instanceof LinkTag) {
                Body body = new Body();
                body.setType(BodyType.VIDEO);
                body.setValue(((LinkTag) node).getAttribute("href"));
                body.setTitle(((LinkTag) node).getLinkText());
                bodys.add(body);

            }

            if (node instanceof ImageTag) {
                Body body = new Body();
                body.setType(BodyType.IMAGE);
                body.setValue(((ImageTag) node).getAttribute("src"));
                bodys.add(body);
            }

            if (node instanceof TextNode) {
                //如果父节点是link标签，此文本节点省去不解析！
                if ((node.getParent() instanceof LinkTag)) {
                    continue;
                }
                Body body = new Body();
                body.setType(BodyType.TXT);
                body.setValue(node.toPlainTextString());
                bodys.add(body);
            }

        }
    }
}

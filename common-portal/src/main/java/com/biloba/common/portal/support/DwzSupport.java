/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.biloba.common.portal.support;

import com.biloba.common.core.util.JacksonUtil;
import com.biloba.common.portal.interceptor.CommonParamInterceptor;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

import static com.biloba.common.portal.support.DWZ.*;


/**
 * @Description: DwzSupport
 * @Author: estn.zuo
 * @CreateTime: 2017-03-22 16:27
 */
public class DwzSupport {

    public static String getRel() {
        return CommonParamInterceptor.mapThreadLocal.get().get(DWZ.REL).toString();
    }

    /**
     * Ajax返回数据格式
     *
     * @param message      提示消息
     * @param callbackType 回调操作
     * @return
     */
    public static String ajax(String message, String callbackType) {
        return ajaxSuccess(message, DwzSupport.getRel(), null, callbackType);
    }

    // 输出JSON,正确消息
    public static String ajaxSuccess(String message, String... params) {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put(STATESCODE, "200");
        jsonMap.put(MESSAGE, message);
        return doWrite(jsonMap, params);
    }

    // 输出JSON,错误消息
    public static String ajaxError(String message, String... params) {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put(STATESCODE, "300");
        jsonMap.put(MESSAGE, message);
        return doWrite(jsonMap, params);
    }

    // 输出JSON,错误消息
    public static String ajaxLogout(String message, String... params) {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put(STATESCODE, "301");
        jsonMap.put(MESSAGE, message);
        return doWrite(jsonMap, params);
    }

    private static String doWrite(Map<String, Object> jsonMap, String[] params) {
        for (int i = 0; i < params.length; i++) {
            if (i + 1 > RESULT_KEYS.length) {
                return JacksonUtil.write(jsonMap);
            }
            if (StringUtils.isBlank(params[i])) {
                continue;
            }
            jsonMap.put(RESULT_KEYS[i], params[i]);
        }
        return JacksonUtil.write(jsonMap);
    }

}

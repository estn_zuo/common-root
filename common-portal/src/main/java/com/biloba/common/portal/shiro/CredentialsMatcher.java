package com.biloba.common.portal.shiro;

import com.biloba.common.core.util.encrypt.BCryptUtil;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * Description: PemassCredentialsMatcher
 * Author: estn.zuo
 * CreateTime: 2014-09-22 11:23
 */
public class CredentialsMatcher extends SimpleCredentialsMatcher {


    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken accountUsernamePasswordToken = (UsernamePasswordToken) token;

        SimpleAuthenticationInfo simpleAuthenticationInfo = (SimpleAuthenticationInfo) info;

        String inputPassword = new String(accountUsernamePasswordToken.getPassword());
        String storePassword = (String) simpleAuthenticationInfo.getCredentials();

        return BCryptUtil.verify(inputPassword, storePassword);

    }
}

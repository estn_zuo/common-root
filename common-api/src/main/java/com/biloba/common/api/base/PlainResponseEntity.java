/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.biloba.common.api.base;

/**
 * @Description: 该实体无需包装直接返回
 * @Author: estn.zuo
 * @CreateTime: 2017-06-02 16:48
 */
public interface PlainResponseEntity {

    String getResult();
}

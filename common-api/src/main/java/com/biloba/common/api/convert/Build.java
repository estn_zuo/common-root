/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.biloba.common.api.convert;

/**
 * @Description: VersionService
 * @Author: estn.zuo
 * @CreateTime: 2017-04-24 22:31
 */
public interface Build {

    String acquireBuild();
}

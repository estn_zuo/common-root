package com.biloba.common.core.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class JacksonUtil extends ObjectMapper {

    private static Logger logger = LoggerFactory.getLogger(JacksonUtil.class);

    private static JacksonUtil instance = new JacksonUtil();

    private JacksonUtil() {

        configure(SerializationFeature.INDENT_OUTPUT, true);

        configure(SerializationFeature.WRAP_ROOT_VALUE, false);

        // 不予输出null字段
        setSerializationInclusion(JsonInclude.Include.NON_NULL);

        configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

        // 当数组中只有一个元素时，也按照数组输出
        configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        // 时间格式化输出
        setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    public static JacksonUtil getInstance() {
        return instance;
    }

    public static String write(Object object) {
        try {
            return getInstance().writeValueAsString(object);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return null;
    }

    public static <T> T read(String content, TypeReference valueType) {
        try {
            return getInstance().readValue(content, valueType);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return null;
    }

    public static <T> T read(String content, Class<T> valueType) {
        try {
            return getInstance().readValue(content, valueType);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return null;
    }


}

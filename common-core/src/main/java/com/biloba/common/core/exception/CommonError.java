package com.biloba.common.core.exception;

public interface CommonError {

    String getErrorCode();

    String getErrorMessage();
}

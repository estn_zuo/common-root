/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.biloba.common.core.util.pattern.handler.concrete;


import com.biloba.common.core.util.pattern.handler.NumberHandler;
import com.biloba.common.core.util.pattern.handler.concrete.number.BigDecimalHandler;
import com.biloba.common.core.util.pattern.handler.concrete.number.DoubleHandler;
import com.biloba.common.core.util.pattern.handler.concrete.number.FloatHandler;
import com.biloba.common.core.util.pattern.handler.concrete.number.IntegerHandler;
import com.biloba.common.core.util.pattern.handler.concrete.number.LongHandler;
import com.biloba.common.core.util.pattern.handler.concrete.number.ShortHandler;

/**
 * @Description: NumberHandlerSupport
 * @Author: estn.zuo
 * @CreateTime: 2017-04-09 21:18
 */
public class NumberHandlerSupport {

    public static Number handle(Object v, Class parameterType) {

        NumberHandler integerHandler = new IntegerHandler();
        NumberHandler longHandler = new LongHandler();
        NumberHandler shortHandler = new ShortHandler();
        NumberHandler bigDecimalHandler = new BigDecimalHandler();
        NumberHandler floatHandler = new FloatHandler();
        NumberHandler doubleHandler = new DoubleHandler();

        integerHandler.setNextHandler(longHandler);
        longHandler.setNextHandler(shortHandler);
        shortHandler.setNextHandler(bigDecimalHandler);
        bigDecimalHandler.setNextHandler(floatHandler);
        floatHandler.setNextHandler(doubleHandler);
        return integerHandler.handleRequest(v, parameterType);
    }
}

package com.biloba.common.core.pojo;

import com.biloba.common.core.serializer.BodySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize(using = BodySerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
public class BodyVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private BodyType type;

    private String value;

    private String title;

    public BodyType getType() {
        return type;
    }

    public void setType(BodyType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}